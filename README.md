---
## **Help**

```
usage: acronis_report.py [-h] [-f {date,fqdn,gateway,message} [{date,fqdn,gateway,message} ...]] [-s {days_aged,date,fqdn,gateway,message}]
                         [-Fs {aged,all,counts,duplicates,fqdn,missing} [{aged,all,counts,duplicates,fqdn,missing} ...]] [-Fd DATE_FILTER] [-Fm MESSAGE_FILTER]
                         [--min-max-age-filter MIN_MAX_AGE_FILTER [MIN_MAX_AGE_FILTER ...]] [-Fg GATEWAY_FILTER] [-Ff FQDN_FILTER] [-Fa] [-l REPORT_DIR] [-d] [--use-fqdn-file]

optional arguments:
  -h, --help            show this help message and exit
  -f {date,fqdn,gateway,message} [{date,fqdn,gateway,message} ...], --filter-by {date,fqdn,gateway,message} [{date,fqdn,gateway,message} ...]
                        Determine how to filter the report
  -s {days_aged,date,fqdn,gateway,message}, --sort-by {days_aged,date,fqdn,gateway,message}
                        Determine how to sort FQDN-based report
  -Fs {aged,all,counts,duplicates,fqdn,missing} [{aged,all,counts,duplicates,fqdn,missing} ...], --display {aged,all,counts,duplicates,fqdn,missing} [{aged,all,counts,duplicates,fqdn,missing} ...]
                        Dictate whether end report has counts, per-fqdn entries, or both
  -Fd DATE_FILTER, --date DATE_FILTER
                        Date to use for filtering (by date). Format is YYYY-MM-DDTHH:MM:SS
  -Fm MESSAGE_FILTER, --message MESSAGE_FILTER
                        Error message to use for filtering
  --min-max-age-filter MIN_MAX_AGE_FILTER [MIN_MAX_AGE_FILTER ...]
                        Minimum (and optionally maximum) age, in days, threshold for a host
  -Fg GATEWAY_FILTER, --gateway GATEWAY_FILTER
                        Gateway to use for filtering
  -Ff FQDN_FILTER, --fqdn-name FQDN_FILTER
                        FQDN(s) to use for filtering
  -Fa, --use-fqdn-report
                        Used aged report FQDN list for filtering
  -l REPORT_DIR, --report-location REPORT_DIR
                        Directory used for the downloaded reports
  -d, --download, --download-report
                        Flag to download report
  --use-fqdn-file       Use a fqdn file with a list of files (should be called fqdn_list.txt)
```
   

---
## **Examples**

**Download a fresh copy of the reports to /tmp/acronis_reports/2022-03-28 directory, and display all sections.  Use the downloaded fqdn report as a fqdn filter for the output. Sort by days_aged column (days since last successful backup, as reported by ammit)**
  
    
```
$ acronis_report.py \
  --download \
  --report-location /tmp/acronis_reports/2022-03-28/ \
  --use-fqdn-report \
  --display all \
  --sort-by days_aged
```
